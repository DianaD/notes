using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

        // Complete the pangrams function below.
        static string pangrams(string s) {
            var voc = "abcdefghijklmnopqrtuvxyzw";
            var not_in=voc.Length;
            var aux = s.ToLower();
            for(int i=0; i<voc.Length; i++)
            {
                for(int j=0; j<aux.Length; j++)
                    if(voc[i]==aux[j])
                    {
                        Console.WriteLine(voc[i]);
                        j=aux.Length+1;
                        not_in--;
                    }
            }
            Console.WriteLine(not_in);
            if(not_in>0)
                return "not pangram";
            else
                return "pangram";
        }


    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        string s = Console.ReadLine();

        string result = pangrams(s);

        textWriter.WriteLine(result);

        textWriter.Flush();
        textWriter.Close();
    }
}
