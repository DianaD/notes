using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Text;
using System;

class Solution {

        // Complete the separateNumbers function below.
        static string funnyString(string s) {

            int[] seq1 = new int[s.Length];
            int[] seq11 = new int[s.Length-1];

            var k = 0;
            for(int i=0; i<=s.Length-1; i++)
            {
                seq1[k]=(int)s[i];
                Console.WriteLine(seq1[k]);
                k++;
            }
            k=0;
            for(int i=0; i<=seq1.Length-2; i++)
            {
                if(seq1[i+1]>seq1[i])
                    seq11[k] = seq1[i+1] - seq1[i];
                else
                    seq11[k] = - seq1[i+1] + seq1[i];
                Console.WriteLine(seq11[k]);
                k++;
            }

            int[] seq2 = new int[s.Length];
            int[] seq21 = new int[s.Length-1];

            k=0;
            for(int i=s.Length-1; i>=0; i--)
            {
                seq2[k]=(int)s[i];
                Console.WriteLine(seq2[k]);
                k++;
            }
            k=0;
            for(int i=0; i<=seq1.Length-2; i++)
            {
                if(seq2[i+1]>seq2[i])
                    seq21[k] = seq2[i+1] - seq2[i];
                else
                     seq21[k] = - seq2[i+1] + seq2[i];
                Console.WriteLine(seq21[k]);
                k++;
            }

            var ok = 0;
            for(int i=0; i<seq1.Length-2; i++)
                if(seq11[i]!=seq21[i])
                    ok++;
            if(ok==0)
                return "Funny";
            else
                return "Not Funny";
        }

    static void Main(string[] args) {
        TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

        int q = Convert.ToInt32(Console.ReadLine());

        for (int qItr = 0; qItr < q; qItr++) {
            string s = Console.ReadLine();

            string result = funnyString(s);

            textWriter.WriteLine(result);
        }

        textWriter.Flush();
        textWriter.Close();
    }
}
