import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {

        int nr_p = 0;
        int nr_n = 0;
        int nr_z = 0;
        for(int i=0; i<arr.length; i++)
        {
            if(arr[i]>0) nr_p++;
            if(arr[i]<0) nr_n++;
            if(arr[i]==0) nr_z++;
        }
        System.out.print(String.format("%.5g%n", nr_p * 1.0 / arr.length));
        System.out.print(String.format("%.5g%n", nr_n * 1.0 /arr.length));
        System.out.print(String.format("%.5g%n", nr_z * 1.0 /arr.length));

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        plusMinus(arr);

        scanner.close();
    }
}
