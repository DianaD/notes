import java.io.*;
import java.math.*;
import java.text.*;
import java.util.*;
import java.util.regex.*;

public class Solution {

    /*
     * Complete the timeConversion function below.
     */
    static String timeConversion(String s) {
        /*
         * Write your code here.
         */
        String value="";
        if(s.charAt(s.length()-2)=='P')
        {
            if(s.charAt(0)=='0')
            {
                if(s.charAt(1)=='1') value="13";
                if(s.charAt(1)=='2') value="14";
                if(s.charAt(1)=='3') value="15";
                if(s.charAt(1)=='4') value="16";
                if(s.charAt(1)=='5') value="17";
                if(s.charAt(1)=='6') value="18";
                if(s.charAt(1)=='7') value="19";
                if(s.charAt(1)=='8') value="20";
                if(s.charAt(1)=='9') value="21";
            }
            if(s.charAt(0)=='1')
            {
                if(s.charAt(1)=='0') value="22";
                else if(s.charAt(1)=='1') value="23";
                else value = s.charAt(0)+""+s.charAt(1);
            }
        }
        if(s.charAt(s.length()-2)=='A')
        {
            if(s.charAt(0)=='1'&&s.charAt(1)=='2') value="00";
            else value=s.charAt(0)+""+s.charAt(1);
        }
        return value+s.substring(2,s.length()-2);
    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scan.nextLine();

        String result = timeConversion(s);

        bw.write(result);
        bw.newLine();

        bw.close();
    }
}
