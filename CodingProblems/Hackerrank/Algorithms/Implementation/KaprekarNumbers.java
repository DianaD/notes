import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

      private static boolean isKaprekar(int num) {

          long squared = (long) num * num;
          String str   = String.valueOf(squared);
          String left  = str.substring(0, str.length() / 2);
          String right = str.substring(str.length() / 2);
          int numL = (left.isEmpty())  ? 0 : Integer.parseInt(left);
          int numR = (right.isEmpty()) ? 0 : Integer.parseInt(right);
          if (numL + numR == num) {
              System.out.print(num + " ");
              return true;
          } else {
              return false;
          }
      }
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int p = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        boolean foundKaprekar = false;
        for (int num = p; num <= q; num++) {
            if (isKaprekar(num)) {
                foundKaprekar = true;
            }
        }
        if (!foundKaprekar) {
            System.out.println("INVALID RANGE");
        }

        scanner.close();
    }
}
