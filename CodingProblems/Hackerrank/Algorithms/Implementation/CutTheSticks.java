import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    // Complete the cutTheSticks function below.
    static int[] cutTheSticks(int[] arr) {

        ArrayList al = new ArrayList();
        ArrayList sizes = new ArrayList();

        for(int i=0; i<arr.length; i++)
            al.add(arr[i]);

        sizes.add(arr.length);
        while(al.size()>0)
        {
            int delete=(int) al.get(al.indexOf(Collections.min(al))); // next to delete
            System.out.println("deleting " + delete);
            for(int i=0; i<al.size(); i++)
            {
                if(al.get(i).equals(delete))
                {
                    al.remove(i);
                    i--;
                }
                else
                    al.set(i,(int)al.get(i)-delete);
            }
            System.out.println("new size " + al.size());
            if(al.size()!=0)
                sizes.add(al.size());
        }

        int[] res = new int[sizes.size()];
        for(int i=0; i<sizes.size(); i++)
            res[i]=(int) sizes.get(i);
        return res;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int[] result = cutTheSticks(arr);

        for (int i = 0; i < result.length; i++) {
            bufferedWriter.write(String.valueOf(result[i]));

            if (i != result.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
