import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {



    // Complete the timeInWords function below.
    static String timeInWords(int h, int m) {

        List numbers = Arrays.asList(
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
            "ten",
            "eleven",
            "twelve",
            "thirteen",
            "fourteen",
            "fifteen",
            "sixteen",
            "seventeen",
            "eighteen",
            "nineteen",
            "twenty",
            "twenty one",
            "twenty two",
            "twenty three",
            "twenty four",
            "twenty five",
            "twenty six",
            "twenty seven",
            "twenty eight",
            "twenty nine"
        );

        if(m<30)
        {
            if(m<1)
                return numbers.get(h) + " o' clock";
            if(m==1)
                return numbers.get(m) + " minute past " + numbers.get(h);
            else if(m!=15)
                return numbers.get(m) + " minutes past " + numbers.get(h);
            if(m==15)
                return "quarter past " + numbers.get(h);
        }
        if(m==30)
            return "half past " + numbers.get(h);
        if(m>30)
        {
            int n = 60-m;
            if(m==1)
                return numbers.get(m) + " minute to " + numbers.get(h);
            if(n!=15)
                return numbers.get(n) + " minutes to " + numbers.get(h+1);
            if(n==15)
                return "quarter to " + numbers.get(h+1);
        }
        return "";
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int h = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int m = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String result = timeInWords(h, m);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
