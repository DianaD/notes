/*
  class Node
    int data;
    Node left;
    Node right;
*/
public static int height(Node root) {
      // Write your code here.
      int lh = -1;
      int rh = -1;
      if(root!=null)
      {
          if(root.left!=null)
              lh=height(root.left);
          if(root.right!=null)
              rh=height(root.right);
      }
      if(lh>rh)
          return lh+1;
      else
          return rh+1;
  }
