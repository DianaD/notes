import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution {

    static int getPos(char a)
    {
        if(a == 'a') return 1;
        else return a-'a'+1;
    }

    static int getIndex(String n)
    {
        int index = 0;
        for(int j=0; j<n.length(); j++)
        {
            int pos = getPos(n.charAt(j));
            if(pos<10)
                index=index*10+pos;
            else
                index=index*100+pos;
        }
        return index;
    }

    // Complete the matchingStrings function below.
    static int[] matchingStrings(String[] strings, String[] queries) {

        HashMap<Integer, Integer> balance = new HashMap();
        int results[] = new int[queries.length];
        for(int i=0; i<strings.length; i++)
        {
            int index = getIndex(strings[i]);
            int newvalue = 0;
            System.out.println(strings[i] + " " + balance.get(index));
            if(balance.get(index)!=null)
            {
                newvalue = balance.get(index)+1;
                balance.replace(index, balance.get(index), newvalue);
            } else
                balance.put(index, 1);
            System.out.println("new value: " + balance.get(index));
        }
        for(int j=0; j<queries.length; j++)
        {
            int index = getIndex(queries[j]);
            if(balance.get(index)!=null)
                results[j]=balance.get(index);
            else
                results[j]=0;
            System.out.println(queries[j] + " " + balance.get(index));
        }
        return results;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int stringsCount = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String[] strings = new String[stringsCount];

        for (int i = 0; i < stringsCount; i++) {
            String stringsItem = scanner.nextLine();
            strings[i] = stringsItem;
        }

        int queriesCount = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String[] queries = new String[queriesCount];

        for (int i = 0; i < queriesCount; i++) {
            String queriesItem = scanner.nextLine();
            queries[i] = queriesItem;
        }

        int[] res = matchingStrings(strings, queries);

        for (int i = 0; i < res.length; i++) {
            bufferedWriter.write(String.valueOf(res[i]));

            if (i != res.length - 1) {
                bufferedWriter.write("\n");
            }
        }

        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
