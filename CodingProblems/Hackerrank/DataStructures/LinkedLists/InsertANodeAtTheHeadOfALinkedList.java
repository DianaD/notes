// Complete the insertNodeAtHead function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
static SinglyLinkedListNode insertNodeAtHead(SinglyLinkedListNode head, int x) {

    SinglyLinkedListNode node = new SinglyLinkedListNode(x);
    node.next = head;
    return node;
}
