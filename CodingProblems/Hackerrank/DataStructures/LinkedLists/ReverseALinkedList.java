// Complete the reverse function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
static SinglyLinkedListNode reverse(SinglyLinkedListNode head) {

    if (head == null){
        return null;
    }

    SinglyLinkedListNode node = null;

        while(head != null){
            SinglyLinkedListNode i = node;
            node = new SinglyLinkedListNode(head.data);
            node.next = i;
            head = head.next;
    }
    return node;
}
