// Complete the insertNodeAtPosition function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
static SinglyLinkedListNode insertNodeAtPosition(SinglyLinkedListNode head, int data, int position) {

   SinglyLinkedListNode node = head;
    int pos = 0;
    while(node.next!=null && pos!=position)
    {
        node = node.next;
        pos++;
        if(pos==position-1)
        {
            SinglyLinkedListNode insert = new SinglyLinkedListNode(data);
            insert.next = node.next;
            node.next = insert;
        }
    }
    return head;

}
