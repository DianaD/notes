// Complete the printLinkedList function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
static void printLinkedList(SinglyLinkedListNode head) {

    if (head == null){
        return;
    }

    SinglyLinkedListNode node = head;
    while(true){
        Console.WriteLine(node.data);
        if (node.next != null){
            node = node.next;
        }
        else {
            break;
        }
    }
}
