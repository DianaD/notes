// Complete the insertNodeAtTail function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
static SinglyLinkedListNode insertNodeAtTail(SinglyLinkedListNode head, int data) {


        if (head == null) {
            SinglyLinkedListNode node = new SinglyLinkedListNode(data);
            head = node;
        } else {
            SinglyLinkedListNode node = head;
            while(node.next != null)
                node = node.next;
            node.next = new SinglyLinkedListNode(data);

        }

    return head;

}
