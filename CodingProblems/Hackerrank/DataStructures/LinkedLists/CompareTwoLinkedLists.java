// Complete the compareLists function below.

/*
 * For your reference:
 *
 * SinglyLinkedListNode {
 *     int data;
 *     SinglyLinkedListNode next;
 * }
 *
 */
static boolean compareLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {

    if((head1 == null && head2!=null)||(head1!=null && head2==null))
        return false;
    else
    {
        SinglyLinkedListNode node1 = head1;
        SinglyLinkedListNode node2 = head2;

        while(node1.next!=null && node2.next!=null)
        {
            if(node1.data!=node2.data)
                return false;
            node1 = node1.next;
            node2 = node2.next;
        }
        if(node1.next!=null || node2.next!=null)
            return false;
        else
            return true;
    }

}
